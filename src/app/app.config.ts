import { environment } from '../environments/environment';


export class AppSettings {

    public static HOST= environment.host;

    public static LANCAMENTOS = '/cielo';

    public static BASE = environment.base;
}