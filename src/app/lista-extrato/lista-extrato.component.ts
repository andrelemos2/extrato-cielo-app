import { Component, OnInit, ViewChild } from '@angular/core';
import { ExtratoService } from '../services/extrato.service';
import { Extrato } from '../model/lancamento';

@Component({
  selector: 'app-lista-extrato',
  templateUrl: './lista-extrato.component.html',
  styleUrls: ['./lista-extrato.component.css']
})
export class ListaExtratoComponent implements OnInit {
  lancamentos: Extrato = {};
  constructor(private extratoService: ExtratoService) {
  }

  displayedColumns = ['dataEfetivaLancamento', 'descricao', 'numero', 'situacao', 'dataConfirmacao', 'dadosBancarios', 'valorFinal'];

  ngOnInit() {
    this.getLancamentos();
  }

  getLancamentos() {
    return this.extratoService
      .getLancamentos()
      .subscribe(lancamentos => {
        this.lancamentos = lancamentos
      });
  }
}
