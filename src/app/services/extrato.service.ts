import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { AppSettings } from '../app.config';
import { Extrato } from '../model/lancamento';

@Injectable()
export class ExtratoService {

  private lancamentosUrl = `${AppSettings.HOST}${AppSettings.LANCAMENTOS}/extrato`;

  constructor(private http: HttpClient) { }

  getLancamentos(): Observable<any> {
    return this.http.get<Extrato[]>(this.lancamentosUrl)
    .pipe(
      tap(_ => this.log('fetched lancamentos')),
      catchError(this.handleError('getLancamentos', []))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }
 
  private log(message: string) {
    console.log(message);
  }
}
