import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaExtratoComponent } from './lista-extrato/lista-extrato.component';

const routes: Routes = [
  {
    path: 'extrato', component: ListaExtratoComponent
  },
  {
    path: '', component: ListaExtratoComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
