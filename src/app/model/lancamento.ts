export interface Extrato {
    dataLancamento?: Date;
    descricao?: String;
    numero?: number;
    situacao?: String;
    dataConfirmacao?: Date;
    dadosBancarios?: String;
    valorFinal?: number;
}